#Implementation of Dictionary (Map) using single list.
#Most originl code by Rance D. Necaise from Data Structures and Algorithms using Python
class Dictionary :
    # Constructor for an empty dictionary reference.
    def __init__(self):
        self._entryList = list()

    #Returns the number of entries contained in the dictionary.
    def __len__(self):
        return len( self._entryList )

    #Looks to see if dictionary contains the given key.
    def __contains__(self, key ):
        ndx = self._findPosition( self, key )
        return ndx is not None

    #Adds a new entry to the dictionary if the key was not found. Otherwise,
    #the new value replaces the current value associated with the key.
    def add( self, key, value ):
        ndx = self._findPosition( key )
        if ndx is not None :             #If key found ...
            self._entryList[ndx].value = value
            return False
        else :
            entry = _DictionaryEntry( key, value )
            self._entryList.append( entry )
            return True

    #Returns the value associated with the key.
    def valueOf(self, key ):
        ndx = self._findPosition( key )
        assert ndx is not None, 'Invalid dictionary entry'
        return self._entryList[ ndx ].value

    #Function allow use of subscripting with this dictionary class.
    def __getitem__(self, key):
        return self.valueOf(key)

    #Function to allow use of item assignment.
    def __setitem__(self, key, value):
        self.add( key, value )

    #Removes entry associated with key.
    def remove( self, key ) :
        ndx = self._findPosition( key )
        assert ndx is not None, 'Invalid dictionary entry'
        self._entryList.pop( ndx )

    # #Returns iterator for traversing keys in dictionary.
    def __iter__( self ) :
        return _DictionaryIterator( self._entryList )

    #Helper function used to find the index position of a category. If the key is
    #not found, None is returned.
    def _findPosition( self, key ):
        #iterate through list looking for key
        for i in range( len( self ) ) :
            #Is the key stored in the ith entry?
            if self._entryList[i].key == key :
                return i
        #If not found, return None
        return None

#Class for storing key/value pairs to be used as Dictionary entries.
class _DictionaryEntry :
    def __init__( self, key, value):
        self.key = key
        self.value = value

#Class implementing dictionary iterator.
class _DictionaryIterator :

    def __init__( self, theList) :
        self.dictItems = theList
        self.current = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.current < len(self.dictItems):
            item = self.dictItems[self.current].key
            self.current += 1
            return item
        else:
            raise StopIteration


