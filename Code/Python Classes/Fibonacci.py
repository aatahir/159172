class Fibs(): # returns 1, 2, 3, 5, 8, 11 ...
    a =0
    b= 0
    def __init__(self):
        self.a = 0
        self.b = 1
    def __next__(self):
        self.a, self.b = self.b, self.a+self.b
        return self.a
    def __iter__(self):
        return self
    def display (self):
        while(self.a<100):
            print(self.a)

        print("the end")


f = Fibs()
f.display()