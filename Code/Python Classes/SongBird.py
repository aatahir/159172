from Classes import Bird
class SongBird(Bird):
    def __init__(self):
        super(SongBird, self).__init__()
        self.sound = 'Sqwark!'
    def sing(self):
        print(self.sound)


b = Bird()
b.__init__()
b.eat()

sb = SongBird()
sb.eat(